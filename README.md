## Pankow Viewers

Companion packge for file viewers to reduce size of the main Pankow package.

Pankow is the UI component library for scaffolding Cloudron apps. It is based on [Vue.js](https://vuejs.org/) using [Vite](https://vitejs.dev/) as the bundler.
The main use-case is to provide common Cloudron related UI compents like a login screen and common layout with header, body and footers.
