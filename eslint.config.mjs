import globals from 'globals';
import js  from '@eslint/js';
import pluginVue from 'eslint-plugin-vue';

export default [
    js.configs.recommended,
    ...pluginVue.configs['flat/essential'],
    {
        files: ["**/*.js"],
        languageOptions: {
            globals: {
                ...globals.browser,
            },
            ecmaVersion: 13,
            sourceType: 'module'
        },
        rules: {
            semi: "error",
            "prefer-const": "error"
        }
    }
];
