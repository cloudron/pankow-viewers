
import GenericViewer from './components/GenericViewer.vue';
import PdfViewer from './components/PdfViewer.vue';
import TextViewer from './components/TextViewer.vue';
import ImageViewer from './components/ImageViewer.vue';

export {
    GenericViewer,
    ImageViewer,
    PdfViewer,
    TextViewer
};

export default {};
