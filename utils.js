

function getFileTypeGroup(item) {
    if (typeof item.mimeType !== 'string') throw 'item must have mimeType string property';
    return item.mimeType.split('/')[0];
}

// named exports
export {
    getFileTypeGroup
};

// default export
export default {
    getFileTypeGroup
};
